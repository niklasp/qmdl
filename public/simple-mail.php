<?php require 'class.simple_mail.php';
 
// Clean up the input values
foreach($_POST as $key => $value) {
  if(ini_get('magic_quotes_gpc'))
    $_POST[$key] = stripslashes($_POST[$key]);
 
  $_POST[$key] = htmlspecialchars(strip_tags($_POST[$key]));
}

 
// Assign the input values to variables for easy reference
$name = $_POST["name"];
$subject = '[qmdl] contact via qmdl.rocks';
$email = $_POST["email"];
$message = $_POST["message"];
 
/* @var SimpleMail $mail */
$mail = SimpleMail::make()
    ->setTo('mail@qmdl.rocks', 'QMDL')
    ->setSubject($subject)
    ->setFrom($email, $name . ' via QMDL Website')
    ->setReplyTo($email, $name)
    ->addGenericHeader('X-Mailer', 'PHP/' . phpversion())
    ->setMessage($message)
    ->setWrap(78);
$send = $mail->send();
//echo $mail->debug();
if ($send) {
    die('Thanks yo! Your mail was sent successfully!');
} else {
    die('An error occurred. We could not send email');
}
 
?>